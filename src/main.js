import { createApp } from 'vue'
import App from './App.vue'
import { VueScrollAnime } from '@goodly/vue-scroll-anime'
import './assets/animie.css'
import './assets/style.css'

const app = createApp(App)
app.use(VueScrollAnime)
app.mount('#app')
